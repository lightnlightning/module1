Rails.application.routes.draw do
  root 'players#index'
  get '/edit', to: 'players#edit'
  get 'login', to: 'players#login'
  patch '/update', to: 'players#update'
  resources :players
end
