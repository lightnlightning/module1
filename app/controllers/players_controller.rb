class PlayersController < ApplicationController
  def index
    @players = Player.all
  end

  def new
    @player = Player.new
  end

  def create
    @player = Player.new(player_params)
    @player.save
    redirect_to player_path(@player.id)
  end

  def show
    @player = Player.find(params[:id])
    # @player = Player.new(params.require(:player).permit(:nickname, :rank))
  end

  def edit
    @player = Player.find_by(params[:id])
  end

  def update
    @player = Player.find_by(params[:id])
    if @player.update(player_params)
      redirect_to @player
    end
  end

  def destroy
    @player = player.find(params[:id])
    @player.destroy

    redirect_to player_path
  end

  private
  def player_params
    params.require(:player).permit(:nickname, :rank, :charisma, :wisdom)
  end
end
